<?php

/**
 * Class App
 * Bootstraps the application.
 * This adds the autoloader, includes extra files etc
 */
class App
{
    public function __construct()
    {
        // Require the always needed files
        require_once('app/helpers/Env.php');
        require_once('app/helpers/Auth.php');
        require_once('app/Autoloader.php');
        require_once('app/DB.php');
        require_once('app/Router.php');
        require_once('app/View.php');

        // Load the env
        Env::load();

        // Set the directories where the autoloader should look
        Autoloader::addDirectory('controllers');
        Autoloader::addDirectory('models');
        Autoloader::addDirectory('middleware');

        //Register all the routes
        Router::initRoutes();
    }

    /**
     * Start the application by calling the router class
     */
    public function start()
    {
        session_start();
        echo Router::dispatch();
    }
}