<?php

class Autoloader
{
    private static $instance = null;
    private $searchDirectories = [];

    /**
     * Set construct method to private because this is an singleton.
     * Register the autoload function.
     * checks the assigned directories for the file with the same name as the class and includes it.
     */
    private function __construct()
    {
        spl_autoload_register(function($className) {
            foreach($this->searchDirectories as $directory) {
                $path = __DIR__ . '/' . $directory . '/' . $className . '.php';
                if(file_exists($path)) {
                    require_once($path);
                    break;
                }
            }
        });
    }

    /**
     * Adds a directory name relative to the app directory for the autoloader to search
     * @param $directory
     */
    public static function addDirectory($directory)
    {
        $instance = self::getInstance();
        $instance->searchDirectories[] = $directory;
    }

    /**
     * Returns the singleton instance
     * @return Autoloader
     */
    public static function getInstance()
    {
        if(!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Set clone method to private because this is an singleton
     */
    private function __clone()
    {
    }
}