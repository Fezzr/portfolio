<?php

class Router
{
    private static $instance = null;
    private $routes = [];
    const ROUTE_DIR = '../routes';

    /**
     * Make the construct method private to make sure we can not create a new instance
     */
    private function __construct()
    {
    }

    /**
     * Make the clone method private to make sure we can not create a new instance
     */
    private function __clone()
    {
    }

    /**
     * Add the route with GET method.
     * This is an easy static method and ports it to the addRoute method.
     *
     * @param String $name
     * @param String $url
     * @param array $parameters
     * @return void
     */
    public static function get(String $name, String $url, Array $parameters)
    {
        $instance = self::getInstance();
        // Explode the url on / so we don't need to do this when checking routes
        $instance->addRoute('get', $name, $url, $parameters);
    }

    /**
     * Returns the singleton instance
     * @return Router
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Add an route to the routes with the given method.
     *
     * @param String $method
     * @param String $name
     * @param String $url
     * @param array $parameters
     */
    private function addRoute(String $method, String $name, String $url, Array $parameters)
    {
        // Explode the url on / so we don't need to do this when checking routes
        $explodedUrl = explode('/', $url);
        $parameters['partials'] = $explodedUrl;
        $parameters['url'] = $url;

        $this->routes[$method][$name] = $parameters;
    }

    /**
     * Add the route with POST method.
     * This is an easy static method and ports it to the addRoute method.
     *
     * @param String $name
     * @param String $url
     * @param array $parameters
     */
    public static function post(String $name, String $url, Array $parameters)
    {
        $instance = self::getInstance();
        $instance->addRoute('post', $name, $url, $parameters);
    }

    /**
     * Create an header that redirects to the given route name
     *
     * @param String $name
     * @return bool
     */
    public static function redirect(String $name)
    {
        $instance = self::getInstance();

        if (isset($instance->routes['get'][$name])) {
            $route = $instance->routes['get'][$name];

            header("location: " . Env::read('BASE_URL') . '/' . $route['url']); //@todo rework this to js version

            return true;
        } else {
            return false;
        }
    }

    /**
     * Return the total url of the route
     *
     * @param String $name
     * @param String $type
     * @return string
     */
    public static function url(String $name = '', String $type = 'get')
    {
        $instance = self::getInstance();

        if ($name === '') {
            return Env::read('BASE_URL');
        } elseif (isset($instance->routes[$type][$name])) {
            $route = $instance->routes[$type][$name];

            return Env::read('BASE_URL') . '/' . $route['url'];
        } else {
            return '';
        }
    }

    /**
     * handle the current url from the path parameter in _GET.
     * Find the corresponding route and invoke the controller,
     * if the route is not found it will throw a 404 error.
     * @return mixed|null
     */
    public static function dispatch()
    {
        $instance = self::getInstance();

        //Get current HTTP method
        $method = $_SERVER['REQUEST_METHOD'];

        //Get current url
        if (isset($_GET['path'])) {
            $path = $_GET['path'];
        } else {
            $path = '';
        }

        // Remove the first / if there is any
        // This is needed for nginx config
        $path = ltrim($path, '/');

        $route = $instance->checkRoute($method, $path);

        if ($route === null) {
            http_response_code(404);
            return null;
        }

        return $instance->runMiddleware($route, function () use ($instance, $route) {
            return $instance->runController($route);
        });
    }

    /**
     * Require all the files in the route directory.
     * In these files all the routes are registered
     */
    public static function initRoutes()
    {
        $files = scandir(self::ROUTE_DIR);

        foreach ($files as $file) {
            if (is_file(self::ROUTE_DIR . '/' . $file)) {
                require_once self::ROUTE_DIR . '/' . $file;
            }
        }
    }

    /**
     * finds the route based on the HTTP method and given url
     *
     * @param string $method
     * @param string $givenUrl
     * @return null
     */
    private function checkRoute(String $method, String $givenUrl)
    {
        $givenUrlPartials = explode('/', $givenUrl);

        foreach ($this->routes[strtolower($method)] as $controlRoute) {
            $params = [];
            $foundRoute = $this->checkRoutePartial($givenUrlPartials, $controlRoute['partials'], $params);
            if ($foundRoute === true) {
                $controlRoute['params'] = $params;
                return $controlRoute;
                break;
            }
        }

        return null;
    }

    /**
     * Check if the partial of the given route equals the current partial of the control route.
     *
     * @param array $givenPartials
     * @param array $controlPartials
     * @param array $params
     * @param int $indent
     * @return bool
     */
    private function checkRoutePartial(Array $givenPartials, Array $controlPartials, Array &$params, Int $indent = 0)
    {
        if (!isset($controlPartials[$indent])) {
            return false;
        }

        //checks if the current controlPartial a parameter
        $isParam = $this->isParameter($controlPartials[$indent], $givenPartials[$indent], $params);

        // We check the next if the partials equals OR if the partial is an parameter
        if ($givenPartials[$indent] == $controlPartials[$indent] || $isParam) {
            if (isset($givenPartials[$indent + 1])) { // Is there a next partial to check?
                return $this->checkRoutePartial($givenPartials, $controlPartials, $params, $indent + 1);
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Check and handle if the partial is an parameter
     *
     * @param String $controlPartial
     * @param String $urlPartial
     * @param array $params
     * @return bool
     */
    private function isParameter(String $controlPartial, String $urlPartial, Array &$params)
    {
        if (preg_match('/^{.*}$/', $controlPartial)) {
            $params[] = $urlPartial;
            return true;
        }

        return false;
    }

    /**
     * Check the route for middleware and run these before running the controller.
     * After all the middleware are run, go back and run the runAfter closure
     *
     * @param array $route
     * @param Closure $runAfter
     * @return mixed
     */
    private function runMiddleware(Array $route, Closure $runAfter)
    {
        if (!isset($route['middleware'])) {
            return $runAfter();
        }
        //We create the middleware in an reversed order since we need an next closure
        $middlewareReversed = array_reverse($route['middleware']);

        $currentNext = $runAfter; // Run the runAfter closure after the last middleware

        foreach ($middlewareReversed as $middleware) {
            $newNext = $this->createMiddlewareClosure($middleware, $currentNext);

            $currentNext = $newNext;
        }

        return $currentNext();
    }

    private function createMiddlewareClosure($className, Closure $next)
    {
        return function () use ($className, $next) {
            $middleware = new $className();

            return $middleware->run($next);
        };
    }

    /**
     * Create the controller class and run the given method.
     *
     * @param array $route
     * @return mixed
     */
    private function runController(Array $route)
    {
        $use = explode('@', $route['use']);

        $controller = new $use[0];
        return call_user_func_array([$controller, $use[1]], $route['params']);
    }
}