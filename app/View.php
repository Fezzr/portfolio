<?php

class View
{
    const VIEW_PATH = './../resources/views/';

    private $path = null;
    private $content = null;

    /**
     * Create a new view object
     * @param String $viewName
     * @param array $parameters
     */
    public function __construct(String $viewName, Array $parameters)
    {
        $parsedViewName = $this->parse($viewName);
        $this->path = self::VIEW_PATH . $parsedViewName . '.php';
        if(file_exists($this->path)) {
            $this->content = $this->getView($parameters);
        } else {
            http_response_code(404);
            //@todo create an 404 view and include him here
            $this->content = null;
        }
    }

    /**
     * Parse dots to backslashes for the pathname
     *
     * @param String $viewName
     * @return mixed
     */
    private function parse(String $viewName)
    {
        return str_replace('.', '/', $viewName);
    }

    /**
     * Get the content of the current view
     * The content is already parsed
     *
     * @param $parameters
     * @return string
     */
    private function getView($parameters)
    {
        foreach($parameters as $key => $parameter) {
            $$key = $parameter;
        }

        ob_start();
        include $this->path;
        return ob_get_clean();
    }

    /**
     * An static method for easy creating and returning an view
     *
     * @param $view
     * @param array $parameters
     * @return null|String
     */
    public static function create(String $view, Array $parameters = [])
    {
        $instance = new self($view, $parameters);

        return $instance->render();
    }

    /**
     * Instead of returning a 'normal' view, return the given data as JSON.
     * Sets the 'content-type' header to json and encode $data to json
     *
     * @param $data
     * @param bool $dontForceObject
     * @return string
     */
    public static function createJsonResponse($data, $dontForceObject = false) {
        // Set the header to json
        header('Content-Type: application/json');

        if($dontForceObject) {
            return json_encode($data);

        } else {
            return json_encode($data, JSON_FORCE_OBJECT);
        }
    }

    /**
     * Return the content of the view
     *
     * @return String|null
     */
    public function render()
    {
        return $this->content;
    }

    /**
     * Include another view in the current view
     * This method should only be used from within an view
     *
     * @param String $name
     * @return string
     */
    public function include(String $name)
    {
        $parsedName = $this->parse($name);
        ob_start();
        include self::VIEW_PATH . $parsedName . '.php';
        return ob_get_clean();
    }

    /**
     * Easy function to return the url from an specific route
     *
     * @param String $name
     * @return string
     */
    public function url(String $name = '')
    {
        return Router::url($name);
    }
}