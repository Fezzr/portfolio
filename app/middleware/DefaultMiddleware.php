<?php

class DefaultMiddleware
{
    public function run(Closure $next)
    {
        // Run what you want to do in this middleware

        return $next(); // Middleware is done
    }
}