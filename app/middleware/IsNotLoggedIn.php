<?php

class IsNotLoggedIn
{
    /**
     * Run the middleware.
     * If the user is logged in, redirect to profile, if not go further
     *
     * @param Closure $next
     * @return bool|mixed
     */
    public function run(Closure $next)
    {
        $user = Auth::getUserData();
        if($user) {
            return Router::redirect('profile');
        } else {
            return $next();
        }
    }
}