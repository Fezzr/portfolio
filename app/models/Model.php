<?php

class Model implements \JsonSerializable
{
    protected $table = null;
    protected $data = [];
    protected $oldData = [];
    protected $exists = false;

    /**
     * Create a new model instance with the given data
     *
     * @param array|null $data
     * @return $this
     */
    public function __construct(Array $data = null)
    {
        if($data !== null) {
            if(isset($data['id'])) {
                $this->exists = true;
            }
            $this->fill($data);
            $this->setOldData();
        }

        return $this;
    }

    /**
     * Fill the model with the given data
     *
     * @param array $data
     * @return $this
     */
    public function fill(Array $data)
    {
        $this->data = array_merge($this->data, $data);

        return $this;
    }

    /**
     * Set the old data field.
     * This field is used for dirty checking
     */
    private function setOldData()
    {
        $this->oldData = $this->data;
    }

    /**
     * Get an array of all the current rows of this model
     * The array exists of new instances of this model
     *
     * @return array
     */
    public static function all()
    {
        // Create a fake instance for the table name etc
        $instance = new static();

        $req = DB::getConnection()->query("SELECT * FROM `{$instance->table}`");
        $list = [];

        foreach($req->fetchAll() as $row) {
            $list[] = new static($row);
        }

        return $list;
    }

    /**
     * Find an row with the given id.
     * if found, a new model of that row is returned
     *
     * @param $id
     * @return static|bool
     */
    public static function find($id)
    {
        // Create a fake instance for the table name etc
        $instance = new static();

        $query = DB::getConnection()->prepare("SELECT * FROM `{$instance->table}` WHERE id = :id");
        $query->execute(['id' => $id]);
        $data = $query->fetch();

        if(!$data) {
            return false;
        } else {
            return new static($data);
        }
    }

    /**
     * Run a multiple select query.
     * $addToQuery is will be concatenated at the end of the query so you can use WHERE, ORDER, LIMIT etc.
     * The result is an array of all the models from the query.
     *
     * @param String $addToQuery
     * @param array $params
     * @return array
     */
    public static function runMultiSelectWithAddOnSql(String $addToQuery, Array $params = [])
    {
        $instance = new static();

        $query = "SELECT * FROM `{$instance->table}` ";
        $query .= $addToQuery;
        $query .= ";";

        return static::runAdvancedMulti($query, $params);
    }

    /**
     * Run a single select query.
     * $addToQuery is will be concatenated at the end of the query so you can use WHERE, ORDER, LIMIT etc.
     * The result is the first model from the query.
     *
     * @param String $addToQuery
     * @param array $params
     * @return bool|Model
     */
    public static function runSingleSelectWithAddOnSql(String $addToQuery, Array $params = [])
    {
        $instance = new static();

        $query = "SELECT * FROM `{$instance->table}` ";
        $query .= $addToQuery;
        $query .= ";";

        return static::runAdvancedSingle($query, $params);
    }

    /**
     * Run a custom query and fetch all rows in an array of model objects
     *
     * @param String $query
     * @param array $params
     * @return array
     */
    public static function runAdvancedMulti(String $query, Array $params = [])
    {
        $stmt = DB::getConnection()->prepare($query);
        $stmt->execute($params);
        $list = [];

        foreach($stmt->fetchAll() as $row) {
            $list[] = new static($row);
        }

        return $list;
    }

    /**
     * Run a custom query and only fetch the first row in an model object
     *
     * @param String $query
     * @param array $params
     * @return bool|static
     */
    public static function runAdvancedSingle(String $query, Array $params = [])
    {
        $stmt = DB::getConnection()->prepare($query);
        $stmt->execute($params);
        $data = $stmt->fetch();

        if(!$data) {
            return false;
        } else {
            return new static($data);
        }
    }

    /**
     * Run a custom query and fetch all rows in an array of model objects.
     * While fetching, merge data with same prefix.
     *
     * @param String $query
     * @param array $params
     * @return array
     */
    public static function runAdvancedMultiWithChildren(String $query, Array $params = [], $parentName, $childName)
    {
        $stmt = DB::getConnection()->prepare($query);
        $stmt->execute($params);
        $list = [];

        // Create the prefix with the names for parent and child
        $parentPrefix = $parentName.'.';
        $childPrefix = $childName.'.';

        foreach($stmt->fetchAll() as $row) {
            $data = static::separateData($row, $parentPrefix, $childPrefix);

            // If the parent object doesn't exist, add it to the list
            if(!array_key_exists($data['parentData']['id'], $list)) {
                $list[$data['parentData']['id']] = new static($data['parentData']);
                $list[$data['parentData']['id']]->$childName = [];
            }

            // Add the child under the current parent
            $currArr = $list[$data['parentData']['id']]->$childName;
            array_push($currArr, new static($data['childData']));
            $list[$data['parentData']['id']]->$childName = $currArr;
        }

        return array_values($list);
    }

    /**
     * Run a custom query and fetch a single row of this model
     * While fetching, merge data with same prefix
     *
     * @param String $query
     * @param array $params
     * @return Model
     */
    public static function runAdvancedSingleWithChildren(String $query, Array $params = [], $parentName, $childName)
    {
        $stmt = DB::getConnection()->prepare($query);
        $stmt->execute($params);
        $dbData = $stmt->fetch();

        // Create the prefix with the names for parent and child
        $parentPrefix = $parentName.'.';
        $childPrefix = $childName.'.';

        $data = static::separateData($dbData, $parentPrefix, $childPrefix);

        $output = new static($data['parentData']);
        $output->$childName =  new static($data['childData']);

        return $output;
    }

    /**
     * Separate the data from a row into child and parent data.
     * This method should only be used by 'runAdvancedMultiWithChildren' and 'runAdvancedSingleWithChildren'.
     *
     * @param $dbData
     * @param $parentPrefix
     * @param $childPrefix
     * @return array
     */
    private static function separateData($dbData, $parentPrefix, $childPrefix) {
        $parentData = [];
        $childData = [];

        // Separate the data between parent and child with the prefixes
        foreach($dbData as $name => $data) {
            // Check if starts with parent prefix or child prefix
            if(substr($name, 0, strlen($parentPrefix)) === $parentPrefix) {
                $newName = substr($name, strlen($parentPrefix));
                $parentData[$newName] = $data;
            } else if(substr($name, 0, strlen($childPrefix)) === $childPrefix) {
                $newName = substr($name, strlen($childPrefix));
                $childData[$newName] = $data;
            }
        }

        return ['parentData' => $parentData, 'childData' => $childData];
    }

    /**
     * Save the current data to the db.
     * The method checks if exists == true to know if it should be an save or create
     * Only fields that are dirty are saved with an update
     *
     * @return bool
     */
    public function save()
    {
        if($this->exists) { //Save is an update
            return $this->performSave();
        } else {
            return $this->performCreate();
        }
    }

    /**
     * Run an update query to save the model
     * This method should only be used by the save method
     *
     * @return bool
     */
    public function performSave()
    {
        // Get only the dirty fields
        $updateFields = $this->getDirty();

        // Dont save if there is no dirty fields
        if(count($updateFields) === 0) {
            return false;
        }

        // Set the head of the statement
        $stmt = "UPDATE `{$this->table}` SET ";

        foreach($updateFields as $field => $data) {
            $stmt .= "`$field`=:$field";

            if($data !== end($updateFields)) {
                $stmt .= ', ';
            }
        }

        // Set the tail of the statement
        $stmt .= " WHERE id = :id";

        $query = DB::getConnection()->prepare($stmt);

        // Add id to the updateFields to prepare the id too
        $updateFields['id'] = $this->data['id'];

        $state = $query->execute($updateFields);

        return $state;
    }

    /**
     * Get all the dirty keys with their values
     *
     * @return array
     */
    public function getDirty()
    {
        $dirty = [];

        foreach($this->data as $field => $data) {
            if(array_key_exists($field, $this->oldData)) { // We can't use isset, since the field can be NULL
                if($data !== $this->oldData[$field]) {
                    $dirty[$field] = $data;
                }
            } else {
                $dirty[$field] = $data;
            }
        }

        return $dirty;
    }

    /**
     * Run a create query to save the model
     * This method should only be used by the save method
     *
     * @return bool
     */
    private function performCreate()
    {
        $keys = array_keys($this->data);
        $keysString = implode(', ', $keys);
        $valueString = implode(', :', $keys);

        $stmt = "INSERT INTO `{$this->table}` ($keysString) VALUES (:$valueString);";
        // Add an extra : at the start of values since we didn't set it with implode

        $query = DB::getConnection()->prepare($stmt);
        $state = $query->execute($this->data);

        if($state) {
            $this->exists = true;
            $this->data['id'] = DB::getConnection()->lastInsertId(); // Set the id of the object

            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete the current model from the database.
     *
     * @return bool
     */
    public function delete()
    {
        if($this->exists) {
            $instance = new static();

            $stmt = "DELETE FROM {$instance->table} WHERE id = :id";

            $query = DB::getConnection()->prepare($stmt);
            $state = $query->execute([
                'id' => $this->data['id']
            ]);

            return $state;
        } else {
            return false; // We can't delete if there is nothing to delete
        }
    }

    /**
     * Checks if the given key is not the same as the given data on instantiation
     *
     * @param String $key
     * @return bool
     */
    public function isDirty(String $key)
    {
        if(isset($this->data[$key])) {
            if(isset($this->oldData[$key])) {
                return $this->data[$key] != $this->oldData[$key];
            } else {
                return true;
            }
        }
        return false;
    }

    /**
     * If the $name parameter is not found, check if it is an key in the data parameter
     *
     * @param String $name
     * @return mixed|null
     */
    public function __get(String $name)
    {
        if(isset($this->data[$name])) {
            return $this->data[$name];
        }

        return null;
    }

    /**
     * Set the given data in the data parameter since it is not an existing parameter of the object
     *
     * @param String $name
     * @param $value
     */
    public function __set(String $name, $value)
    {
        $this->data[$name] = $value;
    }

    /**
     * This method is used to serialize this object to JSON.
     * The method returns the array of all data from the current object
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->data;
    }
}