import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FlexLayoutModule} from "@angular/flex-layout";

import { AboutRoutingModule } from './about-routing.module';
import { AboutComponent } from './about.component';
import {MaterialDesignModule} from "../material-design/material-design.module";

@NgModule({
  imports: [
    CommonModule,
    AboutRoutingModule,
    FlexLayoutModule,
    MaterialDesignModule
  ],
  declarations: [AboutComponent]
})
export class AboutModule { }
