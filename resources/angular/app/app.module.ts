import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FlexLayoutModule} from "@angular/flex-layout";

import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {AboutModule} from './about/about.module';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MaterialDesignModule} from "./material-design/material-design.module";
import {NavComponent} from './nav.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialDesignModule,
    FlexLayoutModule,
    AboutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
