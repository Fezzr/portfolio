import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  routes = [
    {
      title: 'home',
      url: '/'
    },
    {
      title: 'over mij',
      url: '/about'
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
