<!DOCTYPE html>
<html>
<head>
  <title>Portfolio Fake Zijl</title>
  <base href="/">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Mono:300" rel="stylesheet">
</head>
<body>
<app-root></app-root>

<?= $this->include('angular-includes')?>
</body>
</html>
